package com.demo.springboot.service;

import com.demo.springboot.dto.MovieListDto;

public interface MovieService {
    public MovieListDto createGet();
}
